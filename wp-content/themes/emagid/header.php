<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,800" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css" rel="stylesheet">


    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>


	<?php wp_head(); ?>
</head>

<body <?php body_class(''); ?>>
<div id="page" class="site">

	<header>
        <div class="wrapper">
            <nav id="nav_left">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'left-menu'
                    ) );
                ?>
		      </nav>
            <div class="site_branding">
                <a href="/">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slipflop_logo.png" alt="Slip Slop Logo">
                </a>
            </div>
            <nav id="nav_right">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'right-menu'
                    ) );
                ?>
		      </nav>
        </div>
        
        
        
		<div class="site-branding">
		</div><!-- .site-branding -->


	</header><!-- #masthead -->


