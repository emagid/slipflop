<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

if ( ! is_active_sidebar( 'sidebar-2' ) ) {
	return;
}
?>

<aside id="cart_popout" class="widget-area">
    <div class="wrapper">
	   <?php dynamic_sidebar( 'sidebar-3' ); ?>
    </div>
</aside><!-- #secondary -->
