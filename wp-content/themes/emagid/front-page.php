<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

<div class="page_front_container">
    <?php
    get_sidebar('cart');?>
    <section class="fullsize-video-bg">
<!--
        <div class="wrapper">
            <h1><?php the_field('banner_title'); ?></h1>
            <p><?php the_field('banner_text'); ?></p>
            <a href="/product/new-moon/" class="btn btn_transparent">Shop</a>
        </div>
-->
	<div id="video-viewport">
		<video width="1920" height="1280" autoplay muted loop>
			<source src="<?php echo get_template_directory_uri(); ?>/assets/img/intro.mp4" type="video/mp4" />
<!--			<source src="http://www.coverr.co/s3/mp4/Winter-Grass.webm" type="video/webm" />-->
		</video>
	</div>
</section>
    
    
    <script>
    var min_w = 300;
var vid_w_orig;
var vid_h_orig;

$(function() {

    vid_w_orig = parseInt($('video').attr('width'));
    vid_h_orig = parseInt($('video').attr('height'));

    $(window).resize(function () { fitVideo(); });
    $(window).trigger('resize');

});

function fitVideo() {

    $('#video-viewport').width($('.fullsize-video-bg').width());
    $('#video-viewport').height($('.fullsize-video-bg').height());

    var scale_h = $('.fullsize-video-bg').width() / vid_w_orig;
    var scale_v = $('.fullsize-video-bg').height() / vid_h_orig;
    var scale = scale_h > scale_v ? scale_h : scale_v;

    if (scale * vid_w_orig < min_w) {scale = min_w / vid_w_orig;};

    $('video').width(scale * vid_w_orig);
    $('video').height(scale * vid_h_orig);

    $('#video-viewport').scrollLeft(($('video').width() - $('.fullsize-video-bg').width()) / 2);
    $('#video-viewport').scrollTop(($('video').height() - $('.fullsize-video-bg').height()) / 2);

};
    </script>
    <div class="page_front_hero" style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="wrapper">
            <h1><?php the_field('banner_title'); ?></h1>
            <p><?php the_field('banner_text'); ?></p>
            <a href="/product/new-moon/" class="btn btn_transparent">Shop</a>
        </div>
    </div>

    <div class="page_front_layer_top" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/hero_feet.jpg)">
        <div class="overlay">
            <div class="wrapper">
                <div class="page_front_intro">
                    <p><?php the_field('intro_text'); ?></p>
                    <a href="/product/pink-moon/" class="btn">Shop</a>
                </div>
            </div>
        </div>
    </div>

    <div class="page_front_layer_mid">
        <div class="page_front_product_switcher">

            
            <div class="product_slider">

            
<?php
$params = array('posts_per_page' => 5, 'post_type' => 'product');
$wc_query = new WP_Query($params);
?>

     <?php if ($wc_query->have_posts()) : ?>
     <?php while ($wc_query->have_posts()) :
                $wc_query->the_post(); ?>
     <div class="switcher_center">
         <?php the_post_thumbnail(); ?>
         
         <div class="slide_action">
          <h5>
               <a href="<?php the_permalink(); ?>">
               <?php the_title(); ?>
               </a>
            </h5>
                <p>
                    <?php 
                    global $post;
                    $product = new WC_Product($post->ID); 
                    echo     wc_price($product->get_price_including_tax(1,$product->get_price()));
                    ?>
                </p>
         <a href="<?php the_permalink(); ?>" class="btn btn_inverse">View Now</a>
         </div>

     </div>
     <?php endwhile; ?>
     <?php wp_reset_postdata(); ?>
     <?php else:  ?>

          <?php _e( 'No Products' ); ?>

     <?php endif; ?>
            </div>
                

            </div>
<script>
    $(document).ready(function(){
      $('.product_slider').slick({
            centerMode: true,
            slidesToShow: 3,
          arrows: false,
          dots: true,
            responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]

      });
    });
			    
</script>
        

        </div>
    </div>

    <div class="page_front_layer_bottom">
        <div class="wrapper">
            <div class="page_front_process">
                <div class="page_front_process_text">
                    <h3>How it Works</h3>
                    <p>Click below to see the sandal in action!</p>
                    <div class="slip_switch">
                        <div class="slip_off" >
                            <img style="-webkit-filter: grayscale(0%);filter: grayscale(0%);" src="<?php echo get_template_directory_uri(); ?>/assets/img/feet_out.png">
                        </div>
                        <div class="slip_on">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/feet_in.png">
                        </div>
                    </div>
                </div>
                <div class="page_front_process_graphic">
                    <div class="slipped_off">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/slipflop_blk_trans.png">
                    </div>
                    <div class="slipped_on">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/sandle2.png">
                    </div>
                    
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>

<script>
    
$(".slip_on").click(function(){
    $(".slipped_on").slideDown();
    $(".slipped_off").hide();
    $(".slip_on img").css({"-webkit-filter": "grayscale(0%)", "filter": "grayscale(0%)"});
    $(".slip_off img").css({"-webkit-filter": "grayscale(100%)", "filter": "grayscale(100%)"});
});
$(".slip_off").click(function(){
    $(".slipped_off").slideDown();
    $(".slipped_on").hide();
    $(".slip_off img").css({"-webkit-filter": "grayscale(0%)", "filter": "grayscale(0%)"});
    $(".slip_on img").css({"-webkit-filter": "grayscale(100%)", "filter": "grayscale(100%)"});
});
</script>

    <div class="instagram_board">
        <?php echo do_shortcode('[instagram-feed]'); ?>
    </div>
</div>
<?php
get_footer();
