<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	<footer>
        <div class="wrapper">
            <div class="footer_left">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_footer.png">
            </div>
            <div class="footer_center">
                <div class="footer_menu">
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'mobile-menu'
                        ) );
                    ?>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'social-menu'
                        ) );
                    ?>
                </div>
                <div class="footer_news">
                    <p>Sign up for our newsletter to learn more!</p>
                    <?php echo do_shortcode('[contact-form-7 id="34" title="Footer Subscribe"]'); ?>
                </div>
            </div>
        </div>
        <div class="clear"></div>
	</footer>
</div>

<script>
$("li.wpmenucartli").click(function(e){
    e.preventDefault();
    $("aside#cart_popout").animate({width:'toggle'},350);
//    $('.page_front_container').animate({left:"-300px"}, 350);
});
</script>

<?php wp_footer(); ?>

</body>
</html>
