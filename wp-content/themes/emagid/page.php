<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

<div class="page_default_container">
    <div class="default_hero" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/hero_default.jpg)">
        <div class="overlay">
            <div class="wrapper">
                <h1><?php wp_title(''); ?></h1>
            </div>
            
        </div>
    </div>
    

    <div class="default_container">
    

        <div class="default_content">
            <div class="wrapper">
                <h2><?php wp_title(''); ?></h2>
                <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', 'page' );

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;

                endwhile; // End of the loop.
                ?>
            </div>
        </div>
            <?php
    get_sidebar();?>

    </div>
</div>
    
</div>
<?php
get_footer();
