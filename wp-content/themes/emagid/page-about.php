<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

<div class="page_default_container">
    <div class="default_hero" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/hero_feet.jpg)">
        <div class="overlay">
            <div class="wrapper">
                <h1><?php wp_title(''); ?></h1>
            </div>
            
        </div>
    </div>
    

    <div class="default_container">
    

        <div class="default_content" style="width:100%;">
            <div class="wrapper">
                <h2>THE ORIGIN</h2>
                <p>The origin of Slip Flop began on a warm summer day down by the shore. My family and I were enjoying a stroll along the beach, taking in the beautiful scenery. Sunset was soon approaching, and my mother and sister began expressing discomfort of cold toes while wearing their flip flops. We were having such a wonderful time; we did not want to return to the hotel to inconveniently change shoes. Then it hit me! Flashbacks were running through my mind of images from my college career, recalling students wearing flip flops all year long, some looked goofy while they wore socks with their flip flops. All were suffering just to enjoy the simplicity and fashion that flip flops offered.</p>
            </div>
        </div>
        
        <div class="switchbox switchbox_uno"> 
            <div class="switchbox_pic switchbox_pic_left">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/NewMoonPicture2.png">
            </div>
            <div class="switchbox_text"><p>The ease and airy sensation felt by wearing flip-flops is what makes them one of the most illustrious categories of footwear on the market. There’s no other footwear like it out there, until today. We now reveal to you, the innovative world of Slip Flop patented technology! We have evolved the flip-flop into the most supercool, trendy, and versatile footwear known as the Slip Flop.</p></div>
            <div class="clear"></div>
        </div>
        <div class="switchbox"> 
            <div class="switchbox_text"><p>The ease and airy sensation felt by wearing flip-flops is what makes them one of the most illustrious categories of footwear on the market. There’s no other footwear like it out there, until today. We now reveal to you, the innovative world of Slip Flop patented technology! We have evolved the flip-flop into the most supercool, trendy, and versatile footwear known as the Slip Flop.</p></div>
            <div class="switchbox_pic switchbox_pic_right">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/freecountry.png">
            </div>

        </div>
        

</div>
    
    <div class="page_front_hero" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/hero_convert.jpg)">
        <div class="shade">
            <div class="wrapper" style="text-align:center;">
                <h1>Be Cool, Be Warm, Be Ready!</h1>
            </div>
        </div>
    </div>
    
</div>
<?php
get_footer();
